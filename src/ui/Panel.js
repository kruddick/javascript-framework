(function() {

    /* Include statements */
   var Element = include('lib.ui.Element');

    /* Class description */
    define('lib.ui.Panel', {
        extend : Element,
        
        init : function() {
            this.callSuper('div');
            this.addClass('Panel');
            
            var outer = this.outer = new Element('div');
            var inner = this.inner = new Element('div');
            
            inner.appendTo(outer).appendTo(this);
        },
        
        add : function() {
            this.inner.appendChildren.apply(this.inner, arguments);
        }
    });

})(); 